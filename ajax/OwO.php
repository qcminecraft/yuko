<?php
require( dirname(__FILE__) . '/../../../../wp-load.php' );

if(file_exists(__FILE__ . "/OwO.json")){
    http_response_code(301);
    header("Location: OwO.json");
}

if (empty($wpsmiliestrans)) {
    exit("Error");
}

$origin = file_get_contents(dirname(__FILE__) . "/OwO_origin.json");

$result = str_replace("{__template_url__}", get_bloginfo("template_url"), $origin);

echo $result;